class MorseCodeTranslator
  class << self
    def translate(input_string)
      translation = ''

      input_string.split('').each do |char|
        if needs_word_separator?(char)
          translation << '/'
          next
        end

        return "Invalid character '#{char}'" unless can_be_core_character?(char)

        translation << '|' if needs_letter_separator?(translation)

        translation << translated_char(char)
      end

      translation
    end

    private

    CORE_ALPHANUMERICS = {
      'A' => '.-',
      'B' => '-...',
      'C' => '-.-.',
      'D' => '-..',
      'E' => '.',
      'F' => '..-.',
      'G' => '--.',
      'H' => '....',
      'I' => '..',
      'J' => '.---',
      'K' => '-.-',
      'L' => '.-..',
      'M' => '--',
      'N' => '-.',
      'O' => '---',
      'P' => '.--.',
      'Q' => '--.-',
      'R' => '.-.',
      'S' => '...',
      'T' => '-',
      'U' => '..-',
      'V' => '...-',
      'W' => '.--',
      'X' => '-..-',
      'Y' => '-.--',
      'Z' => '--..',
      '0' => '-----',
      '1' => '.----',
      '2' => '..---',
      '3' => '...--',
      '4' => '....-',
      '5' => '.....',
      '6' => '-....',
      '7' => '--...',
      '8' => '---..',
      '9' => '----.'
    }.freeze

    CORE_SPECIAL_CHARS = {
      '.' => '.-.-.-',
      ',' => '--..--'
    }.freeze

    WHITE_SPACE = { ' ' => '/' }.freeze

    def needs_word_separator?(char)
      char == WHITE_SPACE.keys.first
    end

    def needs_letter_separator?(translation)
      translation.size.positive? && translation[-1] != WHITE_SPACE.values.first
    end

    def translated_char(char)
      if CORE_SPECIAL_CHARS.keys.include? char
        CORE_SPECIAL_CHARS[char]
      else
        CORE_ALPHANUMERICS[char.upcase]
      end
    end

    def can_be_core_character?(char)
      (char =~ /[A-Za-z0-9]+/) || CORE_SPECIAL_CHARS.keys.include?(char)
    end
  end
end

require './lib/services/morse_code_obfuscator'
require './lib/services/morse_code_translator'
require 'pry'

class MessageObfuscator
  class << self
    def obfuscate(input)
      return 'Input cannot be blank' if input.size.zero?

      if File.directory?(input) || File.file?(input)
        result = []

        File.open(input).each do |line|
          result << obfuscate_message(line.chomp)
        end

        result.join("\n")
      else
        obfuscate_message input
      end
    end

    private

    def obfuscate_message(message)
      MorseCodeObfuscator.obfuscate(MorseCodeTranslator.translate(message))
    end
  end
end

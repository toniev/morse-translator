This code allows for a certain string or text file to be translated
into an obfuscated morse code version.

To use do:

- `chmod +x bin/message_obfuscator` to allow for the tool to be executed

- make sure you have the specified ruby version

- to obfuscate a single string do `bin/message_obfuscator "Example string."`

- to obfuscate a text file do `bin/message_obfuscator "a/repository/to/file.txt"`

- run `bundle install` to get the needed libraries if you want to run the tests

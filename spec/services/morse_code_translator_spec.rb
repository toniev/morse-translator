require './lib/services/morse_code_translator'

describe MorseCodeTranslator do
  subject { MorseCodeTranslator.translate input }

  context 'when translating a sentence' do
    let(:input) { 'I AM IN TROUBLE' }
    let(:output) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }

    it { is_expected.to eq output }
  end

  context 'when translating a single letter' do
    let(:input) { 'A' }
    let(:output) { '.-' }

    it { is_expected.to eq output }
  end

  context 'when there is an invalid character in the input' do
    let(:input) { 'M&M' }
    let(:invalid_char) { '&' }
    let(:output) { "Invalid character '#{invalid_char}'" }

    it { is_expected.to eq output }
  end
end

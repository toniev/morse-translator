require './lib/services/morse_code_obfuscator'

describe MorseCodeObfuscator do
  shared_examples 'a properly obfuscated input' do
    subject { MorseCodeObfuscator.obfuscate input }

    it { is_expected.to eq output }
  end

  context 'when translating a sentence' do
    let(:input) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }
    let(:output) { '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1' }

    it_behaves_like 'a properly obfuscated input'
  end

  context 'when obfuscating a single letter' do
    let(:input) { '.-' }
    let(:output) { '1A' }

    it_behaves_like 'a properly obfuscated input'
  end

  context 'when there are just dots in a code' do
    let(:input) { '....' }
    let(:output) { '4' }

    it_behaves_like 'a properly obfuscated input'
  end

  context 'when there are just dashes in a code' do
    let(:input) { '---' }
    let(:output) { 'C' }

    it_behaves_like 'a properly obfuscated input'
  end

  context 'when the input is empty' do
    let(:input) { '' }
    let(:output) { '' }

    it_behaves_like 'a properly obfuscated input'
  end
end

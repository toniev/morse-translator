require './lib/services/message_obfuscator'

describe MessageObfuscator do
  subject { MessageObfuscator.obfuscate input }

  context 'when obfuscating a string' do
    let(:input) { 'I AM IN TROUBLE' }
    let(:output) { '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1' }

    it { is_expected.to eq output }
  end

  context 'when obfuscating a file' do
    let(:input) { 'misc/example.txt' }
    let(:output) { "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1" }

    it { is_expected.to eq output }
  end

  context 'when obfuscating an empty line' do
    let(:input) { '' }
    let(:output) { 'Input cannot be blank' }

    it { is_expected.to eq output }
  end

  context 'when obfuscating mixed case lettered message' do
    let(:input) { 'Luke, I am your father.' }
    let(:output) { '1A2|2A|A1A|1|B2B/2/1A|B/A1B|C|2A|1A1/2A1|1A|A|4|1|1A1|1A1A1A' }

    it { is_expected.to eq output }
  end
end
